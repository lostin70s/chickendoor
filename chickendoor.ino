#include "LowPower.h"

//#define _DEBUG


#define IN1  6
#define IN2  7
#define IN3  8
#define IN4  9
int LumPin = A0;
const byte TRIGGER_PIN = 7; // Broche TRIGGER
const byte ECHO_PIN = 8;  // Broche ECHO
#define LED 13
#define MOTOR 10
#define ALIM 2

int dayCeil = 150;
int nightCeil = 50;
boolean initMe;
float meanlum = (float)nightCeil;
float meanlumFact = 0.9f;
int nightCpt=0;
int limIndex = 0;
float nbHour=12;
int maxCpt=(int)(3600.0f*nbHour*1000.0f/8050.0f);

/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 250000UL; // 25ms = ~8m à 340m/s
bool doorStatusOpen = false;

const int stepsPerRevolution = 4048;
float numtour =7.8f;


 
void setup() { //Setup runs once//

    pinMode(IN1, OUTPUT); 
    pinMode(IN2, OUTPUT); 
    pinMode(IN3, OUTPUT); 
    pinMode(IN4, OUTPUT); 
    pinMode(ALIM, OUTPUT);
    pinMode(LED, OUTPUT);
    digitalWrite(LED, LOW);
#ifdef _DEBUG
  dayCeil = 400;
  nightCeil = 300;
#endif
  initMe = false;
#ifdef _DEBUG
  meanlumFact = 0.2f;
#endif
#ifdef _DEBUG
  Serial.begin(115200);
#endif



}


void loop() { //Loop runs forever//



  limIndex = TestLum();
#ifdef _DEBUG
  Serial.print("index  :");
  Serial.print(limIndex);

#endif

  if(doorStatusOpen==true)
    nightCpt++;

  if (limIndex == 1 || nightCpt>maxCpt)
  {
    if (doorStatusOpen == false)
    {
      openDoor();
    }

  }
  if (limIndex == 2)
  {
    if (doorStatusOpen == true)
    {
      closeDoor();
      nightCpt=0;
    }
  }

}


void closeDoor()
{
   #ifdef _DEBUG
  Serial.print("CLose Door");
 
#endif
 
    stepper(stepsPerRevolution*numtour,0);

    delay(1000);
   
    doorStatusOpen=false;
}

void openDoor()
{
 #ifdef _DEBUG
  Serial.print("Open Door");
 
#endif
   
    stepper(stepsPerRevolution*numtour,1);

    delay(1000);

    doorStatusOpen=true;
}

int TestLum()
{

  int lum = analogRead(LumPin);
  meanlum = meanlum * meanlumFact + (1.0f - meanlumFact) * (float)lum;
#ifdef _DEBUG
  Serial.print("lum  :");
  Serial.print(lum);
  Serial.print(" mean:");
  Serial.println(meanlum);
  
#endif
delay(50);
  // waits for a second

#ifdef _DEBUG
  digitalWrite(LED, LOW); // sets the digital pin 13 on
#endif
  digitalWrite(ALIM, LOW);
#ifdef _DEBUG
  LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
#else
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
#endif
  digitalWrite(ALIM, HIGH); // sets the digital pin 13 on
#ifdef _DEBUG
  digitalWrite(LED, HIGH); // sets the digital pin 13 on
#endif



  if (meanlum > dayCeil)
  {

    return 1  ;

  }

  if (meanlum < nightCeil)
  {

    return 2  ;
  }

  return 0;

}



void stepper(int numStep,int direction){
  
  int steps=0;

  if(direction==0)
  steps=7;
  for (int x=0;x<numStep;x++){
    switch(steps){
       case 0:
         digitalWrite(IN1, LOW); 
         digitalWrite(IN2, LOW);
         digitalWrite(IN3, LOW);
         digitalWrite(IN4, HIGH);
       break; 
       case 1:
         digitalWrite(IN1, LOW); 
         digitalWrite(IN2, LOW);
         digitalWrite(IN3, HIGH);
         digitalWrite(IN4, HIGH);
       break; 
       case 2:
         digitalWrite(IN1, LOW); 
         digitalWrite(IN2, LOW);
         digitalWrite(IN3, HIGH);
         digitalWrite(IN4, LOW);
       break; 
       case 3:
         digitalWrite(IN1, LOW); 
         digitalWrite(IN2, HIGH);
         digitalWrite(IN3, HIGH);
         digitalWrite(IN4, LOW);
       break; 
       case 4:
         digitalWrite(IN1, LOW); 
         digitalWrite(IN2, HIGH);
         digitalWrite(IN3, LOW);
         digitalWrite(IN4, LOW);
       break; 
       case 5:
         digitalWrite(IN1, HIGH); 
         digitalWrite(IN2, HIGH);
         digitalWrite(IN3, LOW);
         digitalWrite(IN4, LOW);
       break; 
         case 6:
         digitalWrite(IN1, HIGH); 
         digitalWrite(IN2, LOW);
         digitalWrite(IN3, LOW);
         digitalWrite(IN4, LOW);
       break; 
       case 7:
         digitalWrite(IN1, HIGH); 
         digitalWrite(IN2, LOW);
         digitalWrite(IN3, LOW);
         digitalWrite(IN4, HIGH);
       break; 
       default:
         digitalWrite(IN1, LOW); 
         digitalWrite(IN2, LOW);
         digitalWrite(IN3, LOW);
         digitalWrite(IN4, LOW);
       break; 
    }
  if(direction==1){ steps++;}
  if(direction==0){ steps--; }
  if(steps>7){steps=0;}
  if(steps<0){steps=7; }
  delay(1);
  }
  stopmotor();
} 

void stopmotor()
{
         digitalWrite(IN1, LOW); 
         digitalWrite(IN2, LOW);
         digitalWrite(IN3, LOW);
         digitalWrite(IN4, LOW);
}


 
